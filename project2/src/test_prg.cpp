#include "goldchase.h"
#include "Map.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <mqueue.h>
#include <errno.h>

using namespace std;

//Global variables
struct GameBoard
{
	int row;
	int column;
	pid_t player[5];
	unsigned char map[0];
};

Map *gm; //Map pointer
GameBoard *gb; //GameBoard structure pointer
char currentPlayer; //variable to store the mask of player of this game
//Queue name of players
string queueName[5] = {"/qPlayer1", "/qPlayer2", "/qPlayer3", "/qPlayer4", "/qPlayer5"};
//file descriptor of queue
mqd_t rqfd;
//semaphore
sem_t *goldSem;
//boolean to know when to exit
bool exit_game = false;

//function prototypes
void readFile(string &map, int &goldCount, int &row, int &column);
GameBoard* createSharedMemory(int row, int column);
GameBoard* readSharedMemory();
void insertGold(unsigned char *map, int mapSize, int goldCount);
int insertPlayer(unsigned char *map, int mapSize, char byte);
char availablePlayer(GameBoard *goldMap);
bool readKey(GameBoard *goldMap, Map &goldMine, int &currentPosition, char player, bool &realGold, sem_t *goldSem);
void detectGold(GameBoard *goldMap, Map &goldMine, int currentPosition, bool &realGold); 
void makePlayerAvailable(GameBoard *goldMap, char player);
bool cleanup(GameBoard *goldMap);
void signal_handler(int signal_no);
void notifyPlayers();
void sendMessage();
void readMessage();
void broadcastMessage();
int getMask();
int getPlayerNumber();

/*
	Entry point of program
*/
int main()
{
	//variable declaration
	int goldCount = 0, row = 0, column = 0;
	string wholeMap = "";
	unsigned char *theMine;
	unsigned char *ch;
	GameBoard *goldMap;
	//char currentPlayer;
	int currentPosition;
	bool realGold = false;
	
	//declaring and defining signal handler
	struct sigaction updateSignal;
	updateSignal.sa_handler = signal_handler;
	sigemptyset(&updateSignal.sa_mask);
	updateSignal.sa_flags = 0;
	updateSignal.sa_restorer = NULL;

	sigaction(SIGUSR1, &updateSignal, NULL);
	sigaction(SIGINT, &updateSignal, NULL);
	sigaction(SIGTERM, &updateSignal, NULL);
	sigaction(SIGHUP, &updateSignal, NULL);
	

	//Trying to read semaphore
	goldSem = sem_open("/goldSem", O_RDWR, S_IRUSR|S_IWUSR, 1);

	//if failed in aquiring the semaphore
	if(goldSem == SEM_FAILED)
	{
		//Job of player one
		//create semaphore and aquire lock
		goldSem = sem_open("/goldSem", O_CREAT, S_IRUSR|S_IWUSR, 1);
		
		//reading map file
		//and getting goldCount, row and column
		readFile(wholeMap, goldCount, row, column);
		//getting reference of first character of map
		theMine = (unsigned char*)&wholeMap[0];
		
		//creating shared memory and getting reference of GameBoard structure
		goldMap = createSharedMemory(row, column);
		gb = goldMap; 
		goldMap->row = row;
		goldMap->column = column;
		
		//setting all player to zero - currently no player playing
		for(int i = 0; i < 5; i++)
		{
			goldMap->player[i] = 0;
		}
	
		//wriing map to shared memory
		ch = theMine;
		int index = 0;
		while(*ch != '\0')
		{
			char byte = 0;
			if(*ch == '*')
			{
				byte |= G_WALL;
			}
			goldMap->map[index] = byte;
			index++;
			ch++;
		}

		//insert gold
		insertGold(goldMap->map, (row * column), goldCount);

		//insert player
		currentPlayer = availablePlayer(goldMap);
		currentPosition = insertPlayer(goldMap->map, (row * column), currentPlayer);
		
		//releasing semaphore
		sem_post(goldSem);
	}
	else
	{
		/* For other players - 2nd, 3rd, 4th and 5th */
		sem_wait(goldSem);
		//reading shared memory
		goldMap = readSharedMemory();
		gb = goldMap;

		//inserting player
		currentPlayer = availablePlayer(goldMap);
		if(currentPlayer == 'N')
		{
			cout << "Game arena full. Please try after sometime." << endl;
			cout << "Exiting...." << endl;
			sem_post(goldSem);
			return 0;
		}
		currentPosition = insertPlayer(goldMap->map, (goldMap->row * goldMap->column), currentPlayer);
		sem_post(goldSem);
	}

	//setting up the signal handler for SIGUSR2
	struct sigaction messageSignal;
	messageSignal.sa_handler = signal_handler;
	sigemptyset(&messageSignal.sa_mask);
	messageSignal.sa_flags = 0;
	messageSignal.sa_restorer = NULL;
	
	//registering the handler
	sigaction(SIGUSR2, &messageSignal, NULL);

	//setting the attributes of message attribute
	struct mq_attr mq_attributes;
	mq_attributes.mq_flags = 0;
	mq_attributes.mq_maxmsg = 10;
	mq_attributes.mq_msgsize = 120;
	
	//creating message queue for player - read only mode
	rqfd = mq_open(queueName[getPlayerNumber() - 1].c_str(), O_RDONLY|O_CREAT|O_EXCL|O_NONBLOCK,
		S_IRUSR|S_IWUSR, &mq_attributes);
	if(rqfd == -1)
	{
		perror("ERROR: Error while creating message queue\n");
		exit(1);
	}

	//set up message queue to receive signal whenever message comes in
	struct sigevent mq_notification_event;
	mq_notification_event.sigev_notify=SIGEV_SIGNAL;
	mq_notification_event.sigev_signo=SIGUSR2;
	mq_notify(rqfd, &mq_notification_event);

	//creating object and drawing it on the screen
	sem_wait(goldSem);
	Map goldMine(goldMap->map, goldMap->row, goldMap->column);
	gm = &goldMine;
	notifyPlayers();
	sem_post(goldSem);
	
	goldMine.postNotice("Welcome to Gold Chase");

	bool read = true;
	while((read == true) && (exit_game == false))
	{
		read = readKey(goldMap, goldMine, currentPosition, currentPlayer, realGold, goldSem);
	}
	

	//after quitting the game making the player available
	sem_wait(goldSem);
	makePlayerAvailable(goldMap, currentPlayer);
	if(exit_game)
	{
		int currentColumn = currentPosition % goldMap->column;
		int currentRow = currentPosition / goldMap->column;
		//getting current position of player
		int position = currentPosition;
		goldMap->map[position] &= ~currentPlayer;
		goldMine.drawMap();
		notifyPlayers();
	}

	//checking if the currentPlayer is the last one to exit map
	//then release the shared memory and semaphore
	
	if(cleanup(goldMap))
	{
		sem_post(goldSem);
		//clean up share memory and semaphore before exiting the program
		shm_unlink("/goldChaseMem");
		sem_close(goldSem);
		sem_unlink("/goldSem");
	}
	else
	{
		//releasing semaphore
		sem_post(goldSem);
	}
	//closing message queue
	mq_close(rqfd);
	//unlinking the queue file
	mq_unlink(queueName[getPlayerNumber() - 1].c_str());
	return 0;
}

//Function to read file and return pointer
//of first character for map
void readFile(string &wholeMap, int &goldCount, int &row, int &column)
{
	ifstream file("small_map.txt");
	string line;
	
	//Checking if file can be opened or not
	if(!file.is_open())
	{
		cerr << "ERROR: Unable to read map file." << endl;
		cerr << "Exiting...." << endl;

		exit(1);
	}
	
	//reading file
	//reading first line - number of gold
	getline(file, line);
	goldCount = stoi(line);

	while(getline(file, line))
	{
		//counting columns
		column = line.length();
		wholeMap += line;
		row++; //counting rows
	}
}

//Function to create shared memory
GameBoard* createSharedMemory(int row, int column)
{
	int shm_fd = shm_open("/goldChaseMem", O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);

	int size = (row * column) + sizeof(GameBoard);

	ftruncate(shm_fd, size);

	GameBoard *sharedMem = (GameBoard*) mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, shm_fd, 0);

	return sharedMem;
}

//Function to read shared memory
GameBoard* readSharedMemory()
{
	int shm_fd;
	int row, column;

	shm_fd = shm_open("/goldChaseMem", O_RDWR, S_IRUSR|S_IWUSR);

	read(shm_fd, &row, sizeof(int));
	read(shm_fd, &column, sizeof(int)); 

	return (GameBoard*)mmap(NULL, ((row *column) + sizeof(GameBoard)), PROT_READ|PROT_WRITE, MAP_SHARED, shm_fd, 0);	
}


//Function to insert gold in map
void insertGold(unsigned char *map, int mapSize, int goldCount)
{
	//initializing counter for
	//number of gold palced
	int goldPlaced = 0;

	//seeding random function
	srand(time(NULL));
	//loop until no. of gold placed
	//equals the gold count
	while(goldPlaced < goldCount)
	{
		//generating a random position
		int position = rand() % mapSize;

		//if the position is empty
		if(map[position] == 0)
		{
			//and if the placed gold count is
			// one less than gold count then
			//place fools gold
			if(goldPlaced < (goldCount - 1))
			{
				map[position] = G_FOOL;
			}
			else //place real gold
			{
				map[position] = G_GOLD;
			}
			goldPlaced++;
		}
	}
}

//Function to insert player onto the map
int insertPlayer(unsigned char *map, int mapSize, char byte)
{
	int position = 0;
	//The random generator
	srand(time(NULL));
	
	//loop infinitely until player is placed
	while(1)
	{
		position = rand() % mapSize;

		if(map[position] == 0)
		{
			map[position] |= byte;
			break;
		}
	}
	
	return position;
}

//Function to get the current player
char availablePlayer(GameBoard *goldMap)
{
	for(int i = 0; i < 5; i++)
	{
		if(goldMap->player[i] == 0)
		{
			goldMap->player[i] = getpid();
			if(i == 0)
			{
				return G_PLR0;
			}
			else if(i == 1)
			{
				return G_PLR1;
			}
			else if(i == 2)
			{
				return G_PLR2;
			}
			else if(i == 3)
			{
				return G_PLR3;
			}
			else if(i == 4)
			{
				return G_PLR4;
			}
		}
	}

	return 'N';
}

//Function to make a player available
void makePlayerAvailable(GameBoard *goldMap, char player)
{
	if(player == G_PLR0)
	{
		goldMap->player[0] = 0;
	}
	else if(player == G_PLR1)
	{
		goldMap->player[1] = 0;
	}
	else if(player == G_PLR2)
	{
		goldMap->player[2] = 0;
	}
	else if(player == G_PLR3)
	{
		goldMap->player[3] = 0;
	}
	else if(player == G_PLR4)
	{
		goldMap->player[4] = 0;
	}
}

//Function to decide whether to release
//shared memory and semaphore
bool cleanup(GameBoard *goldMap)
{
	for(int i = 0; i < 5; i++)
	{
		if(goldMap->player[i] == 0)
		{
			//do nothing
		}
		else
		{
			//if any player value is non-zero
			//means it is still playing
			//return false
			return false;
		}
	}
	//when all values are zero
	//mean no one is playing
	//return true
	return true;
}

//Function to move player and check all the boundary conditions 
bool readKey(GameBoard *goldMap, Map &goldMine, int &currentPosition, char player, bool &realGold, sem_t *goldSem)
{
	//reading key from keyboard
	int key = goldMine.getKey();
	
	//getting current column and row of player
	//on map
	//sem_wait(goldSem);
	int currentColumn = 0;
	int currentRow = 0;
	//getting current position of player
	int position = 0;
	//sem_post(goldSem);
	
	switch(key)
	{
		//move left - h
		case 104:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;

			if(currentColumn == 0)
			{
				if(!realGold)
				{
					sem_post(goldSem); //unlocking semaphore
					break;
				}
				else
				{
					goldMap->map[position] &= ~player;
					goldMine.postNotice("You Won!!");
					goldMine.drawMap();
					notifyPlayers();
					sem_post(goldSem); //unlocking semaphore
					return false;
				}
			}
			if(goldMap->map[position - 1] == G_WALL)
			{
				sem_post(goldSem); //unlocking semaphore
				break;
			}
			goldMap->map[position] &= ~player;
			position--;
			goldMap->map[position] |= player;
			currentPosition = position;
			goldMine.drawMap();
			detectGold(goldMap, goldMine, currentPosition, realGold);
			notifyPlayers();
			sem_post(goldSem); //unlocking semaphore
			break;
		//move down - j
		case 106:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;

			if((position + goldMap->column) > (goldMap->row * goldMap->column))
			{
				if(!realGold)
				{
					sem_post(goldSem); //unlocking semaphore
					break;
				}
				else
				{	
					goldMap->map[position] &= ~player;
					goldMine.postNotice("You Won!!");
					goldMine.drawMap();
					notifyPlayers();
					sem_post(goldSem); //unlocking semaphore
					return false;
				}
			}
			if(goldMap->map[position + goldMap->column] == G_WALL)
			{
				sem_post(goldSem); //unlocking semaphore
				break;
			}
			goldMap->map[position] &= ~player;
			position = position + goldMap->column;
			goldMap->map[position] |= player;
			currentPosition = position;
			goldMine.drawMap();
			detectGold(goldMap, goldMine, currentPosition, realGold);
			notifyPlayers();
			sem_post(goldSem); //unlocking semaphore
			break;
		//move up - k
		case 107:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;

			if((position - goldMap->column) < 0)
			{
				if(!realGold)
				{
					sem_post(goldSem); //unlocking semaphore
					break;
				}
				else
				{
					goldMap->map[position] &= ~player;
					goldMine.postNotice("You Won!!");
					goldMine.drawMap();
					notifyPlayers();
					sem_post(goldSem); //unlocking semaphore
					return false;
				}
			}
			if(goldMap->map[position - goldMap->column] == G_WALL)
			{
				sem_post(goldSem); //unlocking semaphore
				break;
			}
			goldMap->map[position] &= ~player;
			position = position - goldMap->column;
			goldMap->map[position] |= player;
			currentPosition = position;
			goldMine.drawMap();
			detectGold(goldMap, goldMine, currentPosition, realGold);
			notifyPlayers();
			sem_post(goldSem); //unlocking semaphore
			break;
		//move right - l
		case 108:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;

			if(currentColumn == (goldMap->column - 1))
			{
				if(!realGold)
				{
					sem_post(goldSem); //unlocking semaphore
					break;
				}
				else
				{
					goldMap->map[position] &= ~player;
					goldMine.postNotice("You Won!!");
					goldMine.drawMap();
					notifyPlayers();
					sem_post(goldSem); //unlocking semaphore
					return false;
				}
			}
			if(goldMap->map[position + 1] == G_WALL)
			{
				sem_post(goldSem); //unlocking semaphore
				break;
			}
			goldMap->map[position] &= ~player;
			position++;
			goldMap->map[position] |= player;
			currentPosition = position;
			goldMine.drawMap();
			detectGold(goldMap, goldMine, currentPosition, realGold);
			notifyPlayers();
			sem_post(goldSem); //unlocking semaphore
			break;
		//to send a message - 'm'
		case 109:
			sendMessage();
			break;
		//to broadcast a message - 'b'
		case 98:
			broadcastMessage();
			break;
		//quit the game - 'Q'
		case 81:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;
			goldMap->map[position] &= ~player;
			goldMine.drawMap();
			notifyPlayers();
			sem_post(goldSem); //unlocking semaphore
			return false;
	}
	return true;
}

//Function to detect if current player position has gold
void detectGold(GameBoard *goldMap, Map &goldMine, int currentPosition, bool &realGold)
{
	if(goldMap->map[currentPosition] & G_FOOL)
	{
		goldMine.postNotice("You found fool's gold!!!");
		goldMine.drawMap();
	}
	else if(goldMap->map[currentPosition] & G_GOLD)
	{
		goldMine.postNotice("You found real gold!!!");
		goldMine.drawMap();
		realGold = true;
	}
}

/*
	Signal handler function for
	SIGUSR1 signal - refresh the screen
	SIGUSR2 signal - read the message from message queue
*/
void signal_handler(int signal_no)
{
	if(signal_no == SIGUSR1)
	{
		gm->drawMap(); //redrawing the map
	}
	else if(signal_no == SIGUSR2)
	{
		readMessage(); //reading messages
	}
	else if((signal_no == SIGINT)||(signal_no == SIGHUP)||(signal_no == SIGTERM))
	{
		exit_game = true;	
	}	
}

/*
	Function to update map
*/
void notifyPlayers()
{
	//looping through the player array of
	//shared memory
	for(int i = 0;i < 5; i++)
	{
		//the player is active
		if(gb->player[i] != 0)
		{
			//send signal to update the screen
			kill(gb->player[i], SIGUSR1);
		}
	}
}

/*
	Function to send message to a
	specific player
*/
void sendMessage()
{
	//getting the bit mask for
	//all the active players
	int mask = getMask();

	//getting the player to whom
	//the message is to be sent
	int player = gm->getPlayer(mask);

	//if only one player is active
	//then break out of function
	if(player == 0)
	{
		return;
	}
	//for some reason the value
	//of variable player for player 3, 4 and 5
	//comes out to 4, 8, 16
	//hence this code to correct it
	else if(player == 4) 
	{
		player = 3;
	}
	else if(player == 8) 
	{
		player = 4;
	}
	else if(player == 16)
	{
		player = 5;
	}
	//getting the message to be sent
	string message = gm->getMessage(); //displaying the prompt
	//concatenating with predefiend text
	message = "Player# " + to_string(getPlayerNumber()) + " says: " + message;
	char *m = &message[0];
	
	//open queue in write mode
	mqd_t wqfd = mq_open(queueName[player - 1].c_str(), O_WRONLY|O_NONBLOCK);
	//if unable to open
	//then print error message and quit
	if(wqfd == -1)
	{
		perror("ERROR: Unable to open queue for writing message\n");
		exit(1);
	}
	
	//message are supposed to be only 120 character long
	//char array is of size 121 to accomodate the
	//extra null character
	char messageText[121];
	memset(messageText, 0, 121); //setting all character of array to '\0'
	strncpy(messageText, m, 120); //copying the message from string to char array
	
	//sending message
	if(mq_send(wqfd, messageText, strlen(messageText), 0) == -1)
	{
		//if unable to ready send message then
		//print error message
		perror("ERROR: Unable to send message!\n");
		//and exit the program
		exit(1);
	}

	//close the message queue descriptor
	mq_close(wqfd);
}

/*
	Function to read message from message queue
	and again setting up the queue to receive whenever
	a new message arrives in queue
*/
void readMessage()
{
	//set up message queue to receive signal whenever
	//a new message arrives
	struct sigevent mq_notification_event;
	mq_notification_event.sigev_notify=SIGEV_SIGNAL;
	mq_notification_event.sigev_signo=SIGUSR2;
	mq_notify(rqfd, &mq_notification_event);

	int err;
	char msg[121]; //message is 120 char long, 1 extra character is for null char
	memset(msg, 0, 121); //set all characters to '\0'
	
	//keep on reading the message from queue until
	//queue is empty
	while((err = mq_receive(rqfd, msg, 120, NULL)) != -1)
	{
		//displaying the message
		gm->postNotice(msg);
		//resetting the char array again to '\0'
		memset(msg, 0, 121);
	}

	//if there is some error then
	//quit the program
	if(errno != EAGAIN)
	{
		perror("ERROR: mq_receive\n");
		exit(1);
	}
}

/*
	Function to broadcast a message to all the active
	players.
*/
void broadcastMessage()
{
	//getting the message to be sent
	string message = gm->getMessage();
	message = "Player# " + to_string(getPlayerNumber()) + ": " + message;
	char *m = &message[0];

	int ignore_player = getPlayerNumber() - 1;
	for(int i = 0; i < 5; i++)
	{
		if((gb->player[i] != 0) && (i != ignore_player))
		{
			//opending the message queue in write mode	
			mqd_t wqfd = mq_open(queueName[i].c_str(), O_WRONLY|O_NONBLOCK);
			//if unable to open the message queue
			//print the message and exit the program
			if(wqfd == -1)
			{
				perror("ERROR: Unable to open queue for writing message\n");
				exit(1);
			}
	
			//message can only 120 characters long
			char messageText[121]; //size of array is 121 counting the last null character
			memset(messageText, 0, 121); //setting all character of array to '\0'
			strncpy(messageText, m, 120); //copy the message in a arrya of size 120
	
			//sending message
			if(mq_send(wqfd, messageText, strlen(messageText), 0) == -1)
			{
				//if unable to send then print error message and
				//and exit the program
				perror("ERROR: Unable to send message!\n");
				exit(1);
			}

			//close the queue
			mq_close(wqfd);
		}
	}
}

/*
	Function to get the bit mask of
	all the available players
*/
int getMask()
{
	int player = 0;
	for(int i = 0; i < 5;i++)
	{
		if(gb->player[i] != 0)
		{
			switch(i)
			{
				case 0:
					if(currentPlayer != G_PLR0)
					{
						player |= G_PLR0;
					}
					break;
				case 1:
					if(currentPlayer != G_PLR1)
					{
						player |= G_PLR1;
					}
					break;
				case 2:
					if(currentPlayer != G_PLR2)
					{
						player |= G_PLR2;
					}
					break;
				case 3:
					if(currentPlayer != G_PLR3)
					{
						player |= G_PLR3;
					}
					break;
				case 4:
					if(currentPlayer != G_PLR4)
					{
						player |= G_PLR4;
					}
					break;
			}
		}
	}

	return player;
}

/*
	Functiont to get the current player
	number
*/
int getPlayerNumber()
{
	if(currentPlayer == G_PLR0)
	{
		return 1;
	}
	if(currentPlayer == G_PLR1)
	{
		return 2;
	}
	if(currentPlayer == G_PLR2)
	{
		return 3;
	}
	if(currentPlayer == G_PLR3)
	{
		return 4;
	}
	if(currentPlayer == G_PLR4)
	{
		return 5;
	}
}
