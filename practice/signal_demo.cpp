#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

using namespace std;

void signal_handler(int signal_number)
{
	cerr << "Caught interrupt signal\n";
}

int main()
{
	struct sigaction signal_action;
	signal_action.sa_handler = signal_handler;
	sigemptyset(&signal_action.sa_mask);
	signal_action.sa_flags = 0;
	signal_action.sa_restorer = NULL;

	sigaction(SIGINT, &signal_action, NULL);

	for(int i = 0; i < 100; i++)
	{
		cout << "Count: " << i << endl;
		sleep(2);
	}
	
	return 0;
}
