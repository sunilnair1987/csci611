#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>

int main()
{
	if(fork() > 0)
	{
		//parent
		exit(0);
	}

	if(setsid() == -1)
	{
		//if unable to set a session
		exit(1);
	}

	//closing all the file descriptors
	for(int i = 0; i < sysconf(_SC_OPEN_MAX); ++i)
	{
		close(i);
	}
	open("/dev/null", O_RDWR); //fd 0
	open("/dev/null", O_RDWR); //fd 1
	open("/dev/null", O_RDWR); //fd 2

	umask(0);
	chdir("/");
	sleep(1000);
}
