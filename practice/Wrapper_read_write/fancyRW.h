/*
 * write template functions that are guaranteed to read and write the 
 * number of bytes desired
 */
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#ifndef fancyRW_h
#define fancyRW_h

template<typename T>
int READ(int fd, T* obj_ptr, int count)
{
	char* addr=(char*)obj_ptr;
	
	int totalBytesRead = 0;
	int bytesRead = 0;
	int remaining = count;

	while((bytesRead = read(fd, addr + totalBytesRead, remaining) != 0))
	{
		if(bytesRead == -1)
		{
			if(errno == EINTR)
			{
				return -1;
			}
		}
		totalBytesRead += bytesRead;
		remaining -= bytesRead;
	}
	return totalBytesRead;
}


template<typename T>
int WRITE(int fd, T* obj_ptr, int count)
{
	char* addr=(char*)obj_ptr;
	int totalBytesWritten = 0;
	int bytesWritten = 0;
	int remaining = count;

	while((bytesWritten = write(fd, addr + totalBytesWritten, remaining) != 0))
	{
		if(bytesWritten == -1)
		{
			if(errno != EINTR)
			{
				return -1;
			}
		}
		totalBytesWritten += bytesWritten;
		remaining -= bytesWritten;
	}
	return totalBytesWritten;
}
#endif
