#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <strings.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int fd[2], nbytes;
	pid_t childpid;
	char string[] = "Hello, World!\n";
	char readBuffer[80];

	//initializing string with all 0
	bzero(readBuffer, 80);

	pipe(fd);

	if((childpid = fork()) == -1)
	{
		perror("fork");
		exit(-1);
	}
	
	if(childpid == 0)
	{
		//child process closes up input side of pipe
		close(fd[0]);

		//send "string" through the output side of pipe
		write(fd[1], string, (strlen(string) + 1));
		exit(0);
	}
	else
	{
		//parent process closes up output side of pipe */
		close(fd[1]);

		//read in a string from pipe
		nbytes = read(fd[0], readBuffer, sizeof(readBuffer));
		printf("Received: %s", readBuffer);
	}

	return 0;
}
