#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char *argv[])
{
	int fd = open("debugpipe", O_RDWR);
	if(fd < 0)
	{
		cerr << "ERROR: Unable to open pipe" << endl;
		exit(1);
	}

	string s = "This is a test message\n";
	int len = strlen(s.c_str());
	int rc = write(fd, s.c_str(), len + 1);
	if(rc < 1)
	{
		cerr << "ERROR: Unable to write to file" << endl;
		exit(0);
	}

	return 0;
}
