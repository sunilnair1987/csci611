#ifndef TESTPRG_H
#define TESTPRG_H

#include "goldchase.h"
#include "Map.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <mqueue.h>
#include <errno.h>
#include <stdlib.h>

using namespace std;

struct GameBoard
{
	int row;
	int column;
	int daemonID;
	pid_t player[5];
	unsigned char map[0];
};


Map *gm; //Map pointer
GameBoard *gb; //GameBoard structure pointer
char currentPlayer; //variable to store the mask of player of this game
//Queue name of players
string queueName[5] = {"/qPlayer1", "/qPlayer2", "/qPlayer3", "/qPlayer4", "/qPlayer5"};
//file descriptor of queue
mqd_t rqfd;
//semaphore
sem_t *goldSem;
//boolean to know when to exit
bool exit_game = false;

//function prototypes
void readFile(string &map, int &goldCount, int &row, int &column);
GameBoard* createSharedMemory(int row, int column);
GameBoard* readSharedMemory();
void insertGold(unsigned char *map, int mapSize, int goldCount);
int insertPlayer(unsigned char *map, int mapSize, char byte);
char availablePlayer(GameBoard *goldMap);
bool readKey(GameBoard *goldMap, Map &goldMine, int &currentPosition, char player, bool &realGold, sem_t *goldSem);
void detectGold(GameBoard *goldMap, Map &goldMine, int currentPosition, bool &realGold); 
void makePlayerAvailable(GameBoard *goldMap, char player);
bool cleanup(GameBoard *goldMap);
void signal_handler(int signal_no);
void notifyPlayers();
void sendMessage();
void readMessage();
void broadcastMessage();
int getMask();
int getPlayerNumber();
#endif
