#ifndef DAEMON_H
#define DAEMON_H

#include <iostream>
#include "Map.h"
#include "goldchase.h"
#include "fancyRW.h"
#include <string>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <mqueue.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string>

using namespace std;

struct GameBoard
{
	int row;
	int column;
	int daemonID;
	pid_t player[5];
	unsigned char map[0];
};

//global variables
int debugFD;
sem_t *daemon_sem;
unsigned char *daemonMap;
unsigned char activePlayers;
int pipefd[2];
const char *portNumber = "25000";
string ipAddress = "";
int socketFD;
GameBoard *gameBoard;

void serverInitialization();
void clientInitialization();
int createServer();
int createClient();
void getActivePlayers();
void setActivePlayers();

/*
	Function to create daemon
*/
void create_daemon(bool isServer)
{
	//opening pipe for communicating with client
	pipe(pipefd);

	if(fork() > 0)
	{
		if(isServer == false)
		{
			close(pipefd[1]); //parent only needs to read
			int rt;
			WRITE(1, "Waiting for shared memory...", sizeof("Waiting for shared memory..."));
			READ(pipefd[0], &rt, sizeof(rt));
			if(rt == 0)
			{
				WRITE(1, "Success!\n", sizeof("Success!\n"));
			}
			else
			{
				WRITE(1, "Failure!\n", sizeof("Success!\n"));	
			}
		}
		return;
	}

	if(fork() > 0)
	{
		//I'm child exit
		exit(0);
	}
	//grandchild
	//set session for grandchild
	if(setsid() == -1)
	{
		exit(1);
	}

	for(int i = 0; i < sysconf(_SC_OPEN_MAX); i++)
	{
		if(i != pipefd[1])
		{
			close(i);
		}
	}
	open("/dev/null", O_RDWR); //fd 0
	open("/dev/null", O_RDWR); //fd 1
	open("/dev/null", O_RDWR); //fd 2

	umask(0);

	chdir("/");

	//opening pipe for debugging
	//debugFD =  open("/home/ubuntu/611/csci611/project3/debugpipe", O_WRONLY);
	
	/*
	Need to register signal handlers
	*/
	
	if(isServer)
	{
		//WRITE(debugFD, "Server daemon successfully created.\n", sizeof("Server daemon successfully created.\n"));
		serverInitialization();	
	}
	else
	{
		//WRITE(debugFD, "Client daemon created successfully.\n", sizeof("Client daemon created successfully.\n"));
		clientInitialization();
	}

}

/*
	Function for server initalization
*/
void serverInitialization()
{
	//WRITE(debugFD, "Inside serverInitialization.\n", sizeof("Inside serverInitialization.\n"));

	//opening semaphore
	daemon_sem = sem_open("/goldSem", O_RDWR, S_IRUSR|S_IWUSR, 1); 

	if(daemon_sem == SEM_FAILED)
	{
		//WRITE(debugFD, "ERROR: Unable to open daemon.\n", sizeof("ERROR: Unable to open daemon.\n"));
		exit(1);
	}
	
	//reading shared memory
	sem_wait(daemon_sem);
	//WRITE(debugFD, "Reading shared memory...", sizeof("Reading shared memory..."));
	int shm_fd;
	int row, column;

	shm_fd = shm_open("/goldChaseMem", O_RDWR | O_CREAT, S_IROTH| S_IWOTH| S_IRGRP| S_IWGRP| S_IRUSR| S_IWUSR);
	
	READ(shm_fd, &row, sizeof(int));
	READ(shm_fd, &column, sizeof(int)); 

	gameBoard = (GameBoard*)mmap(NULL, ((row *column) + sizeof(GameBoard)), PROT_READ|PROT_WRITE, MAP_SHARED, shm_fd, 0);
		
	//WRITE(debugFD, "\n\tRow: ", sizeof("\n\tRow: "));
	//WRITE(debugFD, to_string(row).c_str(), sizeof(to_string(row).c_str()));
	//WRITE(debugFD, "\n", sizeof("\n"));
	//WRITE(debugFD, "\tColumn: ", sizeof("\tColumn: "));
	//WRITE(debugFD, to_string(column).c_str(), sizeof(to_string(column).c_str()));
	//WRITE(debugFD, "\n", sizeof("\n"));
	//WRITE(debugFD, "done\n", sizeof("done\n"));

	
	//setting daemon id
	gameBoard->daemonID = getpid();
	
	int mapSize = row * column;

   daemonMap = new unsigned char[mapSize];
	
	//WRITE(debugFD, "Creating copy of map for daemon...", sizeof("Creating copy of map for daemon..."));
	for(int i = 0; i < mapSize; i++)
	{
		daemonMap[i] = gameBoard->map[i];
	}
	//WRITE(debugFD, "done\n", sizeof("done\n"));
	sem_post(daemon_sem);
	
	//WRITE(debugFD, "Creating server...", sizeof("Creating server..."));
	socketFD = createServer();

	//sending number of row and column to client daemon
	WRITE(socketFD, &row, sizeof(row)); //row
	WRITE(socketFD, &column, sizeof(column)); //column

	//sending map
	//WRITE(debugFD, "Sending map...", sizeof("Sending map..."));
	for(int i = 0; i < mapSize; i++)
	{
		WRITE(socketFD, &daemonMap[i], sizeof(daemonMap[i]));
	}
	//WRITE(debugFD, "done\n", sizeof("done\n"));

	getActivePlayers();
	WRITE(socketFD, &activePlayers, sizeof(activePlayers));	
}

/*
	Function to create a server and wait for the client
	connection. Once client is connected return the file descriptor
*/
int createServer()
{
	int sockfd; //file descriptor for the socket
	int status; //for error checking

	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints)); //zero out everything in structure
	hints.ai_family = AF_UNSPEC; //don't care. Either IPv4 or IPv6
	hints.ai_socktype=SOCK_STREAM; // TCP stream sockets
	hints.ai_flags=AI_PASSIVE; //file in the IP of the server for me

	struct addrinfo *servinfo;
	if((status=getaddrinfo(NULL, portNumber, &hints, &servinfo))==-1)
	{
		fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
		exit(1);
	}
	sockfd=socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);

	/*avoid "Address already in use" error*/
	int yes=1;
	if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))==-1)
	{
		//WRITE(debugFD, "server: setsockopt", sizeof("server: setsockopt"));
		exit(1);
	}

  //We need to "bind" the socket to the port number so that the kernel
  //can match an incoming packet on a port to the proper process
	if((status=bind(sockfd, servinfo->ai_addr, servinfo->ai_addrlen))==-1)
	{
		//WRITE(debugFD, "server: bind", sizeof("server: bind"));
		exit(1);
	}
	//when done, release dynamically allocated memory
	freeaddrinfo(servinfo);

	if(listen(sockfd,1)==-1)
	{
		//WRITE(debugFD, "server: listen", sizeof("server: listen"));
		exit(1);
	}

	//WRITE(debugFD, "done\n", sizeof("done\n"));
	//WRITE(debugFD, "Waiting for client...", sizeof("Waiting for client..."));

	struct sockaddr_in client_addr;
	socklen_t clientSize=sizeof(client_addr);
	int new_sockfd;
	if((new_sockfd=accept(sockfd, (struct sockaddr*) &client_addr, &clientSize))==-1)
	{
		//WRITE(debugFD, "FAILED.\n ERROR: Unable to accept client connection.\n", sizeof("FAILED.\n ERROR: Unable to accept client connection.\n"));
		exit(1);
	}
	//WRITE(debugFD, "CONNECTED\n", sizeof("CONNECTED\n"));
	
	return new_sockfd;	
}

/*
	Function to get Active players
*/
void getActivePlayers()
{
	unsigned char plr = G_SOCKPLR;
	for(int i = 0; i < 5; i++)
	{
		if(gameBoard->player[i] != 0)
		{
			if(i == 0)
			{
				plr |= G_PLR0;
			}
			else if(i == 1)
			{
				plr |= G_PLR1;
			}
			else if(i == 2)
			{
				plr |= G_PLR2;
			}
			else if(i == 3)
			{
				plr |= G_PLR3;
			}
			else if(i == 4)
			{
				plr |= G_PLR4;
			}
		}
	}
	activePlayers = plr;
}

/*
	Function for initializing client daemon
*/
void clientInitialization()
{
	//WRITE(debugFD, "Inside clientInitialization\n", sizeof("Inside clientInitialization\n"));

   //getting socket file descriptor
   //WRITE(debugFD, "Connecting to server....", sizeof("Connecting to server...."));
   socketFD = createClient();
   //WRITE(debugFD, "CONNECTED\n", sizeof("CONNECTED\n"));

   //reading number of row & column from server daemon
   int row = 0, column = 0;
   //row
   READ(socketFD, &row, sizeof(row));
   //column
   READ(socketFD, &column, sizeof(column));

   //WRITE(debugFD, "Row: ", sizeof("Row: "));
   //WRITE(debugFD, to_string(row).c_str(), sizeof(to_string(row).c_str()));
   //WRITE(debugFD, "\n", sizeof("\n"));
   //WRITE(debugFD, "Column: ", sizeof("Column: "));
   //WRITE(debugFD, to_string(column).c_str(), sizeof(to_string(column).c_str()));
   //WRITE(debugFD, "\n", sizeof("\n"));

   //reading map from server
   int mapSize = row * column;

   daemonMap = new unsigned char[mapSize];
   //setting all values to zero
   memset(daemonMap, 0, mapSize);

   for(int i = 0; i < mapSize; i++)
   {
      READ(socketFD, &daemonMap[i], sizeof(daemonMap[i]));
   }

   //reading active players
   READ(socketFD, &activePlayers, sizeof(activePlayers));

	//creating semaphore
   daemon_sem = sem_open("/goldSem", O_CREAT, S_IRUSR|S_IWUSR, 1);

   sem_wait(daemon_sem);
   //WRITE(debugFD, "creating shared memory...", sizeof("creating shared memory..."));
   //creating shared memory
   int shm_fd = shm_open("/goldChaseMem", O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);

   int size = (row * column) + sizeof(GameBoard);

   ftruncate(shm_fd, size);

   gameBoard = (GameBoard*) mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, shm_fd, 0);

   //WRITE(debugFD, "done\n", sizeof("done\n"));

   gameBoard->row = row;
   gameBoard->column = column;
   gameBoard->daemonID = getpid();

   //WRITE(debugFD, "Copying local map to shared memory...", sizeof("Copying local map to shared memory..."));
   for(int i = 0; i < mapSize; i++)
   {
      gameBoard->map[i] = daemonMap[i];
   }
   //WRITE(debugFD, "done\n", sizeof("done\n"));
   //setting active players
   setActivePlayers();

   sem_post(daemon_sem);

   int val = 0;
   WRITE(pipefd[1], &val, sizeof(val));
}

/*
   Function to set active players
*/
void setActivePlayers()
{
   for(int i = 0; i < 5; i++)
   {
      unsigned char plr;
      if(i == 0)
      {
         plr = G_PLR0;
      }
      else if(i == 1)
      {
         plr = G_PLR1;
      }
      else if(i == 2)
      {
         plr = G_PLR2;
      }
      else if(i == 3)
      {
         plr = G_PLR3;
      }
      else if(i == 4)
      {
         plr = G_PLR4;
      }

      if((activePlayers & plr) && (gameBoard->player[i] == 0))
      {
			//WRITE(debugFD, to_string(i).c_str(), sizeof(to_string(i).c_str()));
			//WRITE(debugFD, "\n", sizeof("\n"));
         gameBoard->player[i] = getpid();
      }
   }
}

/*
	Function that creates a client socket
	connectes to the server and returns the
	socket file descriptors
*/
int createClient()
{
	//file descriptor for the socket
   int socket_fd;

   struct addrinfo hints;
   bzero(&hints, sizeof(hints)); //setting everthing to zero in structure
   hints.ai_family = AF_UNSPEC;
   hints.ai_socktype = SOCK_STREAM; //TCP stream

   struct addrinfo *servinfo;
   int returnVal = getaddrinfo(ipAddress.c_str(), portNumber, &hints, &servinfo);
   if(returnVal == -1)
   {
      //WRITE(debugFD, "CLIENT ERROR: getaddrinfo\n", sizeof("CLIENT ERROR: getaddrinfo\n"));
      exit(1);
   }

   socket_fd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);

   /* avoid address already in use error */
   returnVal = connect(socket_fd, servinfo->ai_addr, servinfo->ai_addrlen);
   if(returnVal == -1)
   {
      //WRITE(debugFD, "CLIENT ERROR: Unable to connect with server\n", sizeof("CLIENT ERROR: Unable to connect with server\n"));
      //WRITE(debugFD, &ipAddress, sizeof(ipAddress.c_str()));
      exit(1);
   }

   //release dynamically allocated memory
   freeaddrinfo(servinfo);

   return socket_fd;
}
#endif
