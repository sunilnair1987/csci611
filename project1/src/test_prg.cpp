#include "goldchase.h"
#include "Map.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

using namespace std;

//Global variables
struct GameBoard
{
	int row;
	int column;
	int player[5];
	unsigned char map[0];
};

//function prototypes
void readFile(string &map, int &goldCount, int &row, int &column);
GameBoard* createSharedMemory(int row, int column);
GameBoard* readSharedMemory();
void insertGold(unsigned char *map, int mapSize, int goldCount);
int insertPlayer(unsigned char *map, int mapSize, char byte);
char availablePlayer(GameBoard *goldMap);
bool movePlayer(GameBoard *goldMap, Map &goldMine, int &currentPosition, char player, bool &realGold, sem_t *goldSem);
void detectGold(GameBoard *goldMap, Map &goldMine, int currentPosition, bool &realGold); 
void makePlayerAvailable(GameBoard *goldMap, char player);
bool cleanup(GameBoard *goldMap);

/*
	Entry point of program
*/
int main()
{
	//variable declaration
	int goldCount = 0, row = 0, column = 0;
	string wholeMap = "";
	unsigned char *theMine;
	unsigned char *ch;
	sem_t *goldSem;
	GameBoard *goldMap;
	char currentPlayer;
	int currentPosition;
	bool realGold = false;

	//Trying to read semaphore
	goldSem = sem_open("/goldSem", O_RDWR, S_IRUSR|S_IWUSR, 1);

	//if failed in aquiring the semaphore
	if(goldSem == SEM_FAILED)
	{
		//Job of player one
		//create semaphore and aquire lock
		goldSem = sem_open("/goldSem", O_CREAT, S_IRUSR|S_IWUSR, 1);
		
		//reading map file
		//and getting goldCount, row and column
		readFile(wholeMap, goldCount, row, column);
		//getting reference of first character of map
		theMine = (unsigned char*)&wholeMap[0];
		
		//creating shared memory and getting reference of GameBoard structure
		goldMap = createSharedMemory(row, column); 
		goldMap->row = row;
		goldMap->column = column;
		cout << "Column: " << goldMap->column << ", Row: " << goldMap->row << endl;
		
		//setting all player to zero - currently no player playing
		for(int i = 0; i < 5; i++)
		{
			goldMap->player[i] = 0;
		}
	
		//wriing map to shared memory
		ch = theMine;
		int index = 0;
		while(*ch != '\0')
		{
			char byte = 0;
			if(*ch == '*')
			{
				byte |= G_WALL;
			}
			goldMap->map[index] = byte;
			index++;
			ch++;
		}

		//insert gold
		insertGold(goldMap->map, (row * column), goldCount);

		//insert player
		currentPlayer = availablePlayer(goldMap);
		currentPosition = insertPlayer(goldMap->map, (row * column), currentPlayer);
		
		//releasing semaphore
		sem_post(goldSem);
	}
	else
	{
		/* For other players - 2nd, 3rd, 4th and 5th */
		sem_wait(goldSem);
		//reading shared memory
		goldMap = readSharedMemory();

		//inserting player
		currentPlayer = availablePlayer(goldMap);
		if(currentPlayer == 'N')
		{
			cout << "Game arena full. Please try after sometime." << endl;
			cout << "Exiting...." << endl;
			sem_post(goldSem);
			return 0;
		}
		currentPosition = insertPlayer(goldMap->map, (goldMap->row * goldMap->column), currentPlayer);
		sem_post(goldSem);
	}

	//creating object and drawing it on the screen
	sem_wait(goldSem);
	Map goldMine((const char*)goldMap->map, goldMap->row, goldMap->column);
	sem_post(goldSem);
	
	goldMine.postNotice("Welcome to Gold Chase");

	bool playing = true;
	while(playing)
	{
		playing = movePlayer(goldMap, goldMine, currentPosition, currentPlayer, realGold, goldSem);
	}
	

	//after quitting the game making the player available
	sem_wait(goldSem);
	makePlayerAvailable(goldMap, currentPlayer);
	sem_post(goldSem);
	
	//checking if the currentPlayer is the last one to exit map
	//then release the shared memory and semaphore
	sem_wait(goldSem);
	if(cleanup(goldMap))
	{
		sem_post(goldSem);
		//clean up share memory and semaphore before exiting the program
		shm_unlink("/goldChaseMem");
		sem_close(goldSem);
		sem_unlink("/goldSem");
	}
	else
	{
		//releasing semaphore
		sem_post(goldSem);
	}
	return 0;
}

//Function to read file and return pointer
//of first character for map
void readFile(string &wholeMap, int &goldCount, int &row, int &column)
{
	ifstream file("maze.txt");
	string line;
	
	//Checking if file can be opened or not
	if(!file.is_open())
	{
		cerr << "ERROR: Unable to read map file." << endl;
		cerr << "Exiting...." << endl;

		exit(1);
	}
	
	//reading file
	//reading first line - number of gold
	getline(file, line);
	goldCount = stoi(line);

	while(getline(file, line))
	{
		//counting columns
		column = line.length();
		wholeMap += line;
		row++; //counting rows
	}
}

//Function to create shared memory
GameBoard* createSharedMemory(int row, int column)
{
	int shm_fd = shm_open("/goldChaseMem", O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);

	int size = (row * column) + sizeof(GameBoard);

	ftruncate(shm_fd, size);

	GameBoard *sharedMem = (GameBoard*) mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, shm_fd, 0);

	return sharedMem;
}

//Function to read shared memory
GameBoard* readSharedMemory()
{
	int shm_fd;
	int row, column;

	shm_fd = shm_open("/goldChaseMem", O_RDWR, S_IRUSR|S_IWUSR);

	read(shm_fd, &row, sizeof(int));
	read(shm_fd, &column, sizeof(int)); 

	return (GameBoard*)mmap(NULL, ((row *column) + sizeof(GameBoard)), PROT_READ|PROT_WRITE, MAP_SHARED, shm_fd, 0);	
}


//Function to insert gold in map
void insertGold(unsigned char *map, int mapSize, int goldCount)
{
	//initializing counter for
	//number of gold palced
	int goldPlaced = 0;

	//seeding random function
	srand(time(NULL));
	//loop until no. of gold placed
	//equals the gold count
	while(goldPlaced < goldCount)
	{
		//generating a random position
		int position = rand() % mapSize;

		//if the position is empty
		if(map[position] == 0)
		{
			//and if the placed gold count is
			// one less than gold count then
			//place fools gold
			if(goldPlaced < (goldCount - 1))
			{
				map[position] = G_FOOL;
			}
			else //place real gold
			{
				map[position] = G_GOLD;
			}
			goldPlaced++;
		}
	}
}

//Function to insert player onto the map
int insertPlayer(unsigned char *map, int mapSize, char byte)
{
	int position = 0;
	//The random generator
	srand(time(NULL));
	
	//loop infinitely until player is placed
	while(1)
	{
		position = rand() % mapSize;

		if(map[position] == 0)
		{
			map[position] |= byte;
			break;
		}
	}
	
	return position;
}

//Function to get the current player
char availablePlayer(GameBoard *goldMap)
{
	for(int i = 0; i < 5; i++)
	{
		if(goldMap->player[i] == 0)
		{
			goldMap->player[i] = 1;
			if(i == 0)
			{
				return G_PLR0;
			}
			else if(i == 1)
			{
				return G_PLR1;
			}
			else if(i == 2)
			{
				return G_PLR2;
			}
			else if(i == 3)
			{
				return G_PLR3;
			}
			else if(i == 4)
			{
				return G_PLR4;
			}
		}
	}

	return 'N';
}

//Function to make a player available
void makePlayerAvailable(GameBoard *goldMap, char player)
{
	if(player == G_PLR0)
	{
		goldMap->player[0] = 0;
	}
	else if(player == G_PLR1)
	{
		goldMap->player[1] = 0;
	}
	else if(player == G_PLR2)
	{
		goldMap->player[2] = 0;
	}
	else if(player == G_PLR3)
	{
		goldMap->player[3] = 0;
	}
	else if(player == G_PLR4)
	{
		goldMap->player[4] = 0;
	}
}

//Function to decide whether to release
//shared memory and semaphore
bool cleanup(GameBoard *goldMap)
{
	for(int i = 0; i < 5; i++)
	{
		if(goldMap->player[i] == 0)
		{
			//do nothing
		}
		else
		{
			//if any player value is non-zero
			//means it is still playing
			//return false
			return false;
		}
	}
	//when all values are zero
	//mean no one is playing
	//return true
	return true;
}
//Function to move player and check all the boundary conditions 
bool movePlayer(GameBoard *goldMap, Map &goldMine, int &currentPosition, char player, bool &realGold, sem_t *goldSem)
{
	//reading key from keyboard
	int key = goldMine.getKey();
	
	//getting current column and row of player
	//on map
	//sem_wait(goldSem);
	int currentColumn = 0;
	int currentRow = 0;
	//getting current position of player
	int position = 0;
	//sem_post(goldSem);
	
	switch(key)
	{
		//move left - h
		case 104:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;

			if(currentColumn == 0)
			{
				if(!realGold)
				{
					sem_post(goldSem); //unlocking semaphore
					break;
				}
				else
				{
					goldMap->map[position] &= ~player;
					goldMine.postNotice("You Won!!");
					goldMine.drawMap();
					sem_post(goldSem); //unlocking semaphore
					return false;
				}
			}
			if(goldMap->map[position - 1] == G_WALL)
			{
				sem_post(goldSem); //unlocking semaphore
				break;
			}
			goldMap->map[position] &= ~player;
			position--;
			goldMap->map[position] |= player;
			currentPosition = position;
			goldMine.drawMap();
			detectGold(goldMap, goldMine, currentPosition, realGold);
			sem_post(goldSem); //unlocking semaphore
			break;
		//move down - j
		case 106:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;

			if((position + goldMap->column) > (goldMap->row * goldMap->column))
			{
				if(!realGold)
				{
					sem_post(goldSem); //unlocking semaphore
					break;
				}
				else
				{	
					goldMap->map[position] &= ~player;
					goldMine.postNotice("You Won!!");
					goldMine.drawMap();
					sem_post(goldSem); //unlocking semaphore
					return false;
				}
			}
			if(goldMap->map[position + goldMap->column] == G_WALL)
			{
				sem_post(goldSem); //unlocking semaphore
				break;
			}
			goldMap->map[position] &= ~player;
			position = position + goldMap->column;
			goldMap->map[position] |= player;
			currentPosition = position;
			goldMine.drawMap();
			detectGold(goldMap, goldMine, currentPosition, realGold);
			sem_post(goldSem); //unlocking semaphore
			break;
		//move up - k
		case 107:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;

			if((position - goldMap->column) < 0)
			{
				if(!realGold)
				{
					sem_post(goldSem); //unlocking semaphore
					break;
				}
				else
				{
					goldMap->map[position] &= ~player;
					goldMine.postNotice("You Won!!");
					goldMine.drawMap();
					sem_post(goldSem); //unlocking semaphore
					return false;
				}
			}
			if(goldMap->map[position - goldMap->column] == G_WALL)
			{
				sem_post(goldSem); //unlocking semaphore
				break;
			}
			goldMap->map[position] &= ~player;
			position = position - goldMap->column;
			goldMap->map[position] |= player;
			currentPosition = position;
			goldMine.drawMap();
			detectGold(goldMap, goldMine, currentPosition, realGold);
			sem_post(goldSem); //unlocking semaphore
			break;
		//move right - l
		case 108:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;

			if(currentColumn == (goldMap->column - 1))
			{
				if(!realGold)
				{
					sem_post(goldSem); //unlocking semaphore
					break;
				}
				else
				{
					goldMap->map[position] &= ~player;
					goldMine.postNotice("You Won!!");
					goldMine.drawMap();
					sem_post(goldSem); //unlocking semaphore
					return false;
				}
			}
			if(goldMap->map[position + 1] == G_WALL)
			{
				sem_post(goldSem); //unlocking semaphore
				break;
			}
			goldMap->map[position] &= ~player;
			position++;
			goldMap->map[position] |= player;
			currentPosition = position;
			goldMine.drawMap();
			detectGold(goldMap, goldMine, currentPosition, realGold);
			sem_post(goldSem); //unlocking semaphore
			break;
		//quit the game - 'Q'
		case 81:
			sem_wait(goldSem); //locking semaphore
			currentColumn = currentPosition % goldMap->column;
			currentRow = currentPosition / goldMap->column;
			//getting current position of player
			position = currentPosition;
			goldMap->map[position] &= ~player;
			goldMine.drawMap();
			sem_post(goldSem); //unlocking semaphore
			return false;
	}
	return true;
}

//Function to detect if current player position has gold
void detectGold(GameBoard *goldMap, Map &goldMine, int currentPosition, bool &realGold)
{
	if(goldMap->map[currentPosition] & G_FOOL)
	{
		goldMine.postNotice("You found fool's gold!!!");
		goldMine.drawMap();
	}
	else if(goldMap->map[currentPosition] & G_GOLD)
	{
		goldMine.postNotice("You found real gold!!!");
		goldMine.drawMap();
		realGold = true;
	}
} 
